import Link from "next/link";
import Image from "next/image";

export const Logo = ({ width, height }: { width: number; height: number }) => {
    return (
        <Link href="/">
            <Image
                src="/images/logo.png"
                alt="SUZU GROUP LOGO"
                height={height}
                width={width}
            ></Image>
        </Link>
    );
};
