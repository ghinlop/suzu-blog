"use client";

import { useRouter } from "next/navigation";

export const SubMenu = () => {
    const router = useRouter();

    return (
        <div className="grid grid-cols-2 gap-2 font-bold">
            <button
                onClick={() => router.push("/login")}
                className="w-[121px] h-[62px] rounded-[18px] p-[18px_27px]"
            >
                Login
            </button>
            <button
                onClick={() => router.push("/register")}
                className="w-[121px] h-[62px] rounded-[18px] p-[18px_27px] text-white bg-blog-primary"
            >
                Register
            </button>
        </div>
    );
};
