"use client";

import { ROUTER } from "@/utils/routers";
import Link from "next/link";
import { usePathname } from "next/navigation";

export const MainMenu = () => {
    const pathname = usePathname();

    return (
        <ul className="flex-grow flex items gap-14 font-medium">
            {ROUTER.map((v, i) => {
                let activeClass = pathname === v.url ? "text-blog-menu-active" : "";

                return (
                    <li key={i} className="text-blog-menu">
                        <Link href={v.url} className={activeClass}>
                            {v.label}
                        </Link>
                    </li>
                );
            })}
        </ul>
    );
};
