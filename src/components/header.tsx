import { MainMenu } from "./menu/main_menu";
import { SubMenu } from "./menu/sub_menu";
import { Logo } from "./logo";
import { Inter } from "next/font/google";

const inter = Inter({ subsets: ["latin", "vietnamese"] });

export const Header = ({}) => {
    return (
        <header className={inter.className}>
            <div className="container">
                <div className="border-b border-[#CBCBCB]">
                    <div className="py-8 xl:pt-12 xl:pb-3 md:pb-4">
                        <nav className="flex items-center xl:gap-x-28 gap-x-20">
                            <Logo width={112} height={42} />
                            <MainMenu />
                            <SubMenu />
                        </nav>
                    </div>
                </div>
            </div>
        </header>
    );
};
