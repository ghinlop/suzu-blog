import React from "react";
import { Header } from "@/components/header";

export default function RootLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <div className="flex flex-col min-h-dvh">
            <Header />
            <main>{children}</main>
        </div>
    );
}
