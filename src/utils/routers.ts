export const ROUTER = [
    {
        label: "Home",
        url: "/",
    },
    {
        label: "Menu",
        url: "/menu",
    },
    {
        label: "Pricing",
        url: "/pricing",
    },
    {
        label: "Contact",
        url: "/contact",
    },
];
